import React from 'react';

const Table = ({ tableData }) => {
  return (
    <div className="card">
      <table data-testid="table">
        <thead>
          <tr>
            <th>Country</th>
            <th>Capital</th>
          </tr>
        </thead>
        <tbody id="table-body" data-testid="tableBody">
          {tableData.map((d, i) => {
            return (
              <tr key={d.country + d.city + i}>
                <td>{d.country}</td>
                <td>{d.city}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
