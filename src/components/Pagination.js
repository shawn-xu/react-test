import React, { useState } from 'react';
import Table from './Table';

const Pagination = ({ data }) => {
  // States
  const dataLen = data.length;
  const [size, setSize] = useState(5);
  const [page, setPage] = useState(0);

  const buttons = Array(Math.ceil(dataLen / size))
    .fill(null)
    .map((_, index) => {
      return (
        <button key={index} onClick={() => onPageChange(index)}>{`${
          index + 1
        }`}</button>
      );
    });
  const tableData = data.slice(page * size, page * size + size);

  // Event Handlers
  const onSizeChange = event => {
    const value = event.target.value;
    setSize(value);
  };
  const onPageChange = page => {
    setPage(page);
  };
  return (
    <>
      <div className="card flex justify-content-center align-items-center px-50 py-40 my-20 input-div">
        <form onSubmit={e => e.preventDefault()}>
          <select onChange={onSizeChange} data-testid="selectInput">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
          </select>
        </form>

        <div className="button-div" data-testid="buttonDiv">
          {buttons}
        </div>
      </div>

      <Table tableData={tableData} />
    </>
  );
};

export default Pagination;
